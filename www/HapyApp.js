
function HapyApp() {
}


HapyApp.prototype.getImages = function(filePath, sucess) {
	var args = {};

	args.filePath = filePath;

	cordova.exec(function(j){
		if(typeof j != "undefined"){

			sucess(JSON.parse(j));

		}else{
			sucess({});
		}
	}, function(err){

		console.error(err);

	}, "Command", "getImages", [args]);
}

HapyApp.prototype.getMusics = function(filePath, sucess) {
	var args = {};

	args.filePath = filePath;

	cordova.exec(function(j){
		if(typeof j != "undefined"){

			sucess(JSON.parse(j));

		}else{
			sucess({});
		}
	}, function(err){

		console.error(err);

	}, "Command", "getMusics", [args]);
}

HapyApp.prototype.getMusicAlbums = function(filePath, sucess) {
	var args = {};

	args.filePath = filePath;

	cordova.exec(function(j){
		if(typeof j != "undefined"){

			sucess(JSON.parse(j));

		}else{
			sucess({});
		}
	}, function(err){

		console.error(err);

	}, "Command", "getMusicAlbums", [args]);
}

HapyApp.prototype.playMusic = function(filePath, sucess) {
	var args = {};

	args.filePath = filePath;

	cordova.exec(function(j){
		if(typeof j != "undefined"){

			sucess(JSON.parse(j));

		}else{
			sucess({});
		}
	}, function(err){

		console.error(err);

	}, "Command", "playMusic", [args]);
}

HapyApp.prototype.uploadImages = function(url,filePath,key,params,header,sucess,error) {
	var args = {};

	args.url = url;
	args.key = key;
	args.filePath = filePath;
    args.header = header;
    args.params = params;

	cordova.exec(sucess, error, "Command", "uploadImages", [args]);
}

HapyApp.prototype.echo = function(message) {
	var args = {};

	args = message;

	cordova.exec(function(j){
		alert(j);
		console.log(j);
	}, function(err){
		alert("Error!");
		console.error(err);

	}, "Command", "echo", [args]);
}

cordova.addConstructor(function()  {
	if(!window.plugins)
	{
		window.plugins = {};
	}

   // shim to work in 1.5 and 1.6
   if (!window.Cordova) {
   	window.Cordova = cordova;
   };

   window.HapyApp = new HapyApp();
});
