package com.hapyapp.plugins;

import com.hapyapp.plugins.FileSearch;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.File;

public class Image{
	
	public JSONArray getImages(String path) throws JSONException{
		
		// try{
			
			FileSearch fs = new FileSearch();
			
			File parentPath = new File(path.replace("file://",""));
		
			JSONArray arr = new JSONArray(fs.getListFiles(parentPath,new String[]{".png",".jpeg",".jpg"}));
			
			return arr;
			
		// }catch(JSONException e){
			
			// return null;
			
		// }
		
		
		
	}
	
}