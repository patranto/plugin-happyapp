package com.hapyapp.plugins;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.hapyapp.plugins.FileSearch;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.media.MediaMetadataRetriever;
import android.util.Base64;
import java.io.OutputStream;
import android.media.MediaPlayer;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.Context;
import android.net.Uri;


public class Music{

	public JSONArray getMusics(String path,Context context) throws JSONException {
		
		File pathFind = new File(path.replace("file://",""));
		
		FileSearch fs = new FileSearch();
		
		JSONArray arr = new JSONArray(fs.getListFiles(pathFind,new String[]{".mp3"}));
		
		return this.getID3(arr,context);
		
		
	}
	
	public JSONObject getMusicsAlbums(String path,Context context) throws JSONException {
		
		JSONArray arr = this.getMusics(path,context);
		
		JSONObject listAlbum = new JSONObject();
		
		for(int i = 0; i < arr.length(); i++){
			
			JSONObject obj = arr.getJSONObject(i);
			JSONObject tags = new JSONObject();
			
			JSONObject song = new JSONObject();
			
			String albumName = "Unknown";
			String titleName = "Unknown";
			if(obj.has("artwork")){
				
				tags.put("artwork",obj.getString("artwork"));
				
			}
			
			if(obj.has("artist")){
				
				tags.put("artist",obj.getString("artist"));
				
			}
			
			if(obj.has("title")){
				titleName = obj.getString("title");
			}
			
			song.put("title",titleName);
			song.put("path",obj.getString("path"));
			
			if(obj.has("album")){
				
				albumName = obj.getString("album");
				
			}
			
			if(listAlbum.has(albumName)){
				
				JSONObject temp = listAlbum.getJSONObject(albumName);
				JSONArray listSong = new JSONArray();
				
				if(temp.has("songs")){
					listSong = temp.getJSONArray("songs");
					temp.remove("songs");
				}
				
				temp.put("songs",listSong);
				
				listAlbum.put(albumName,temp);
				
			}else{
				JSONArray arrTemp = new JSONArray();
				
				arrTemp.put(song);
				
				tags.put("songs",arrTemp);
				
				listAlbum.put(albumName,tags);
				
			}
			
			
			
		}
		
		return listAlbum;
		
	}
	
	
	
	public JSONArray getID3(JSONArray pathMusic,Context context) throws JSONException {
		
		JSONArray arr = new JSONArray();
		
		MediaMetadataRetriever mr = new MediaMetadataRetriever();
		
		String cachePath = Uri.fromFile(context.getExternalCacheDir()).toString() + '/';
		
		for(int i = 0; i < pathMusic.length() ;i++){
			
			JSONObject tags = new JSONObject();
			
			String path = pathMusic.getString(i);
			
			try{
				mr.setDataSource(path.replace("file://",""));
			
				String albumName = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
				
				tags.put("album",albumName);
				
				String artistName = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
				
				tags.put("artist",artistName);
				
				String title = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
				
				tags.put("title",title);
				
				String author = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
				
				tags.put("author",author);
				
				try{
					
					byte[] artwork = mr.getEmbeddedPicture();
					
					Bitmap artworkBitmap = BitmapFactory.decodeByteArray(artwork,0,artwork.length);
				
					FileOutputStream out = null;
					
					try{
						
						String artworkPath = cachePath.replace("file://","") + albumName;
						
						out = new FileOutputStream(artworkPath);
						
						artworkBitmap.compress(Bitmap.CompressFormat.JPEG,35,out);
						
						System.out.println(artworkPath);
						
						tags.put("artwork","file://" + artworkPath);
						
					}catch(Exception e){
						
					}finally{
						try{
							if( out != null){
								out.close();
							}
						}catch(IOException e){
							
						}
					}
					
				}catch(Exception e){
					
				}
				
				
				
				
			}catch(Exception e){
				
			}
			
			tags.put("path",path);
			
			arr.put(tags);
			
		}
		
		return arr;
		
	}
	
	public JSONObject playMusic(String path) throws IOException, JSONException {
		
		JSONObject jb = new JSONObject();
		
		MediaPlayer mp = new MediaPlayer();
		System.out.println(path.replace("file://",""));
		mp.setDataSource(path.replace("file://",""));
		mp.prepare();
		mp.start();
		
		jb.put("success",200);
		
		return jb;
		
	}

}