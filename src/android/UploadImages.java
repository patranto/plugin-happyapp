package com.hapyapp.plugins;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONTokener;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.io.File;
import java.nio.charset.Charset;

public class UploadImages{
	
	private static final String BOUNDARY =  "+++++";
	
	public String upload(String urlPath,String key,JSONArray path) throws JSONException {
		
		String result = "";
		
		try{
			HttpClient httpclient = new DefaultHttpClient();

			HttpPost httppost = new HttpPost(urlPath);
			
			// httppost.addHeader("Content-Type", "multipart/form-data; boundary=\""+BOUNDARY+"\";");
			
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			
			for(int i = 0;i < path.length(); i++){
				
				String filePath = path.getString(i);
				
				
				
				File f = new File(filePath.replace("file://",""));
				reqEntity.addPart(key + "["+ i +"]",new FileBody(f));
					
			}
			
			httppost.setEntity(reqEntity);
			HttpResponse response = httpclient.execute(httppost);
			
			result = EntityUtils.toString(response.getEntity());
			
		}catch(ClientProtocolException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		System.out.println(result);
		
		return result;
	}
	
}