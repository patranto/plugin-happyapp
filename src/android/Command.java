package com.hapyapp.plugins;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.lang.Thread;

import com.hapyapp.plugins.Image;
import com.hapyapp.plugins.Music;

import com.hapyapp.plugins.UploadImages;

public class Command extends CordovaPlugin{
	
	@Override
	public boolean execute(final String action,final JSONArray args,final CallbackContext callbackContext) throws JSONException{
		
		
		 
		if(action.equals("getImages")){
			
			new Thread(new Runnable() {	
	
				public void run(){
					try{
											
						Image img = new Image();
						
						JSONObject obj = args.getJSONObject(0);
						
						JSONArray resp = img.getImages(obj.getString("filePath"));
						
						if(resp.toString() != null){
						
							callbackContext.success(resp.toString());
							
						}else{
							
							callbackContext.error("Error Occured");
							
						}
						
					}catch(JSONException e){
						
						callbackContext.error("Error Occured");
						
					}
				}
					
			}).start();

			
			
	
		}else if(action.equals("getMusics")){
			
			// cordova.getActivity().runOnUiThread(new Runnable() {

				// public void run(){
				
			try{
									
				Music music = new Music();
				
				JSONObject obj = args.getJSONObject(0);
				
				JSONArray resp = music.getMusics(obj.getString("filePath"),cordova.getActivity());
				
				if(resp != null){
				
					callbackContext.success(resp.toString());
					
				}else{
					
					callbackContext.error("Error Occured");
					
				}
				
			}catch(JSONException e){
				
				callbackContext.error("Error Occured");
				
			}
		
			return true;
			
				// }
					
			// });
			
		}else if(action.equals("getMusicAlbums")){
			
			new Thread(new Runnable() {
				public void run(){
					try{
									
						Music music = new Music();
						
						JSONObject obj = args.getJSONObject(0);
						
						JSONObject resp = music.getMusicsAlbums(obj.getString("filePath"),cordova.getActivity());
						
						
						if(resp != null){
						
							callbackContext.success(resp.toString());
							
						}else{
							
							callbackContext.error("Error Occured");
							
						}
						
					}catch(JSONException e){
						
						callbackContext.error(e.getMessage());
						
					}
				}
			}).start();
			
			
			return true;
			
		}else if(action.equals("playMusic")){
			
			try{
									
				Music music = new Music();
				
				
				try{
					
					JSONObject obj = args.getJSONObject(0);
			
					JSONObject resp = music.playMusic(obj.getString("filePath"));
					
					
					if(resp != null){
				
						callbackContext.success(resp.toString());
						
					}else{
						
						callbackContext.error("Error Occured");
						
					}
				}catch(IOException e){
					callbackContext.error(e.getMessage());
				}
				
				
				
				
				
			}catch(JSONException e){
				
				callbackContext.error(e.getMessage());
				
			}
		
			return true;
		}else if(action.equals("uploadImages")){
			
			// new Thread(new Runnable(){
				
				// public void run(){
					
					UploadImages upload = new UploadImages();
					
					try{
						JSONObject obj = args.getJSONObject(0);
					
						String res = upload.upload(obj.getString("url"),obj.getString("key"),obj.getJSONArray("filePath"));
						
						if(!res.isEmpty()){
							callbackContext.success(res);
						}else{
							callbackContext.error("error");
						}
					}catch(JSONException e){
						e.printStackTrace();
						callbackContext.error("Error");
					}
					
				// }
				
			// }).start();			
			
		
			return true;
		}
		
			
		return true;
	}
	
}
