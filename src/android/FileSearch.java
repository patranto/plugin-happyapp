package com.hapyapp.plugins;

import java.util.ArrayList;
import java.util.List;
import java.io.File;

class FileSearch{
	
	
	// public ArrayList<String> getListFiles(File parentDir){
		
		// return this.getListFiles(parentDir,new String[]{"*"});
		
	// }
	
	public ArrayList<String> getListFiles(File parentDir,String[] ext) {
			
		ArrayList<String> inFiles = new ArrayList<String>();
		
		File[] files = parentDir.listFiles();
		
		for (File file : files) {
			if (file.isDirectory()) {
				inFiles.addAll(getListFiles(file,ext));
			} else {
				if(this.checkExt(file.getName(),ext)){
					inFiles.add("file://" + file.getAbsolutePath());
				}
			}
		}
		
		return inFiles;
	}
	
	
	private boolean checkExt(String fileName,String[] exts){
		
		boolean isAllow = false;
		
		for (String ext : exts){
			
			// System.out.println(ext);
			
			if(ext.equals("*")){			
				isAllow = true;
			}else{
				if(fileName.contains(ext)){
					isAllow = true;
				}
			}
			
		}
		
		return isAllow;
		
		
	}
	
}