#import <Cordova/CDV.h>

@interface Command : CDVPlugin

-(void)echo:(CDVInvokedUrlCommand*)command;
-(void)getImages:(CDVInvokedUrlCommand*)command;
-(void)uploadImages:(CDVInvokedUrlCommand*)command;

@end
