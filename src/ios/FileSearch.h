#import <Foundation/Foundation.h>

@interface FileSearch:NSObject

-(NSArray*)getFiles:(NSString*)filePath withExt:(NSArray*)ext;

@end
