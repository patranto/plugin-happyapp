#import <Foundation/Foundation.h>

@interface UploadImages:NSObject

-(NSString*)upload:(NSString*)urlServer attach:(NSArray*)attachedFiles setKey:(NSString*)key setHeader:(NSDictionary*)header setParams:(NSDictionary*)params;

@end
