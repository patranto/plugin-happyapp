#import <Foundation/Foundation.h>
#import "UploadImages.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@implementation UploadImages

-(NSString*)upload:(NSString*)urlServer attach:(NSArray*)attachedFiles setKey:(NSString*)key setHeader:(NSDictionary*)header setParams:(NSDictionary*)params
{

  NSURL* url = [NSURL URLWithString:urlServer];
  NSString* response = nil;
  ASIFormDataRequest *request = [[ASIFormDataRequest alloc] initWithURL:url];

  if([attachedFiles count] > 0){
    for(int i = 0;i < [attachedFiles count];i++){

      [request setFile:[[attachedFiles objectAtIndex:i] stringByReplacingOccurrencesOfString:@"file://" withString:@"" ] forKey:[NSString stringWithFormat:@"%@[%d]",key,i]];

    }

  }
    
    for(NSString* key in header){
        NSString* val = header[key];
        
        [request addRequestHeader:key value:val];
    }
    
    for(NSString* key in params){
        NSString* val = params[key];
        
        [request setPostValue:val forKey:key];
    }

  [request setTimeOutSeconds:200];

  [request startSynchronous];

  NSError* error = [request error];

  if(!error){
    response = [request responseString];
  }else{
    response = nil;
  }

  // NSLog(@"Error %@", [request responseString]);

  // response = [attachedFiles description];

  return response;

}

@end
