#import "FileSearch.h"

@implementation FileSearch

- (NSArray*)getFiles:(NSString*)filePath withExt:(NSArray*)ext
{


  NSURL *bundleRoot = [NSURL URLWithString:filePath];

  NSFileManager *fm = [NSFileManager defaultManager];
  NSArray *dirContents = [fm contentsOfDirectoryAtPath:filePath error:nil];

  return dirContents;

}

@end
