#import "Command.h"
#import <Cordova/CDV.h>
#import "FileSearch.h"
#import "UploadImages.h"

@implementation Command

-(void)echo:(CDVInvokedUrlCommand*)command
{
  CDVPluginResult* pluginResult = nil;
  NSString* message = [command.arguments objectAtIndex:0];

  if(message != nil && [message length] > 0){
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
  }else{
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
  }

  [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)getImages:(CDVInvokedUrlCommand*)command
{
  CDVPluginResult* pluginResult = nil;
  NSDictionary* args = [command.arguments objectAtIndex:0];

  NSString* filePath = args[@"filePath"];

  FileSearch* files = [[FileSearch alloc] init];

  NSArray* fileList = [files getFiles:filePath withExt:@[ @".jpg", @".jpeg",@".png" ]];

  NSLog(@"File List %@",[fileList description]);

  NSURL *urlPath = [[NSBundle mainBundle] bundleURL];

  NSLog(@"File Root Url %@",[urlPath description]);

  [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"TEST"] callbackId:command.callbackId];
}

-(void)uploadImages:(CDVInvokedUrlCommand*)command
{

  [self.commandDelegate runInBackground:^{
    CDVPluginResult* pluginResult = nil;
    NSDictionary* args = [command.arguments objectAtIndex:0];

    NSString* url = args[@"url"];
    NSString* key = args[@"key"];
    NSArray* filePath = args[@"filePath"];
      NSDictionary* params = args[@"params"];
      
      NSDictionary* header = args[@"header"];


    UploadImages* uploadImages = [[UploadImages alloc] init];

      NSString* response = [uploadImages upload:url attach:filePath setKey:key setHeader:header setParams:params];

    if(response != nil && [response length] > 0){
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:response];

      NSLog(@"Response %@",response);
    }else{
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
  }];
}

@end
